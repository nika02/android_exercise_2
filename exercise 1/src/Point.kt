import kotlin.math.abs

class Point (var x: Double, var y: Double) {

    override fun toString(): String {
        return "x: ${x}, y: ${y}."
    }

    override fun equals(other: Any?): Boolean {
        if(other == null || other !is Point || x != other.x || y != other.y)
            return false
        return true
    }

    fun symmetricallyTowardsTheHead() {
        x = -x;
        y = -y;
    }

    fun lengthBetween(point2: Point): Double {
        var width = abs(x - point2.x);
        var length = abs(y - point2.y);

        return Math.sqrt((width*width) + (length*length))
    }
}