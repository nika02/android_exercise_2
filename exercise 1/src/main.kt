fun main() {
    println("hello")

    val p1 = Point(5.0, 0.0)
    val p2 = Point(0.0, 5.0)

    println(p1.toString())
    p1.symmetricallyTowardsTheHead()
    println(p1.toString())
    p1.symmetricallyTowardsTheHead()
    println(p1.toString())

    println(p1.equals(p2))

    println(p1.lengthBetween(p2))
}