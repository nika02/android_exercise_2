fun main() {
    var car1 = CarFactory().create("Audi")
    var car2 = CarFactory().create("Maserati")
    var car3 = CarFactory().create("Mercedes")

    var airCraft1 = AircraftFactory().create("Boeing737")
    var airCraft2 = AircraftFactory().create("Boeing777")
    var airCraft3 = AircraftFactory().create("Boeing007")

    car1?.drive()
    car2?.drive()
    car3?.drive()

    airCraft1?.fly()
    airCraft2?.fly()
    airCraft3?.fly()
}